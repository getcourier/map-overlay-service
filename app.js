var express = require( 'express' ),
    _ = require( 'lodash' ),
    path = require('path'),
    fs = require('mz/fs'),
    del = require('del'),
    gm = require('gm').subClass({ imageMagick: true }),
    polyline = require('polyline'),
    request = require('request-promise'),
    AWS = require('aws-sdk'),
    smooth = require('chaikin-smooth');

var environment = require( './env' );

// http://localhost:2047/?photo=https://dgtzuqphqg23d.cloudfront.net/kunNCEaormyqSW1xuZ0sKSIrHzusxYZw1PiWQPH1eDY-576x768.jpg&polyline=gi`gGydb`@gAWDuDbEcE]qJbEfBlAaB{BsF~BYuA}HxBhAFcCpBbFpChAxAvFqCzk@bAvD_BPcIkVwIcARwElDoCOaJ|D~AdAuAuBkGbCQsAuHxB|@g@oBt@CnIbNiCpm@~@hDyAt@oIuWaG@&id=123456789
// http://localhost:2047/?photo=https://dgtzuqphqg23d.cloudfront.net/6muBaoZNtgLX4Mu7Rymm46UsYzwNSkxSjKTky2Ugb5o-768x432.jpg&polyline=e`fuHnt\\iNgI}LuO{NtPwS~Oo^~b@__@pJaLtJej@~u@sIvWkHpJ}f@lhAyQzJoP|V_Xjm@_Uz`@aEfPyK{SyZixAyBVsGzHqObXeGdFkJdCkUoCcObLpDfT[tOhO}@bNqVzCnB~C~XvGpEnEdUlC~i@vEda@gCrB|CfP|CbAmBxM_@`zA}Fjk@S~_@hAjNoFvu@`AdVlEdRx@fU`ZxTlThYbQpCtBhEwCpm@kHnVyFlD}Gff@Fpb@|CvQtIwLhDyKzVcXpWoPJ}IbEaObGoAdB{HxC{DjLDhIfL~HcGjOy^tK{KdWaAbo@}Zre@g@`WmUpLmF`GgJdGu`@aGwE_@gCePeFhBo]yGwAGoB`K}`BHgPlEiUbCi[fBoC|FdAXeFzCh@jA{D`EF^wlAjHqiAnBmq@uAmFx@eNwFaEhJoq@vDydA|CVlBim@IqGgAeAhMgmB&id=123456793
// http://localhost:2047/?photo=https://dgtzuqphqg23d.cloudfront.net/eHxf3x4flGZqxO8ru5MhS3aMRXPpgyFSmlU94FOqFZo-768x576.jpg&polyline=ikguHxw[qH`bA{WbrAKne@hFWPuCr@Tg@|BoDnAcBII_SqNcJcJf@gLbOuB_FcBvAoHkIkSnCcQcE}GgF{EbByEbMqG{B_MsQcSen@qO_\\u@gKRsFjCoFpH{J`EeBb`@`@bUpMrJvB`PaMdKMbVyG`HV?fBi@yBhOkB`GoFxGqNzFr@gAtEZpEvGiKtEkB|G{I&id=123456794

// Local variables
var app,
    photoFilename = 'photo.jpg',
    overlayFilename = 'overlay.png',
    compositeFilename = 'composite.jpg';

/**
 * Init
 */
var init = function() {
  var app = express();

  app.get('/', index );
  app.get('/compose', runner );

  // Start the server
  app.listen( environment.port, function () {
    console.info( "Server started on port " + environment.port );
  } );

  AWS.config.update( {
      accessKeyId: environment.s3.accessKeyId,
      secretAccessKey: environment.s3.secretAccessKey
  } );

};

var multiplier = 10000;

var getPolylineBounds = function( polyline ) {
  var bounds = {
        lat: { min: multiplier, max: 0 },
        long: { min: multiplier, max: 0 }
      };

  // Get mins/maxes of lats/longs
  polyline.forEach( function( pair ) {
    pair.forEach( function( value, i ) {
      if ( i % 2 ) { // Long
        bounds.long.min = Math.min( bounds.long.min, value );
        bounds.long.max = Math.max( bounds.long.max, value );
      } else { // Lat
        bounds.lat.min = Math.min( bounds.lat.min, value );
        bounds.lat.max = Math.max( bounds.lat.max, value );
      }
    } );
  } );

  return bounds;
};

var translatePolyline = function( polyline ) {
  var bounds = getPolylineBounds( polyline );

  var minLat = Math.floor( bounds.lat.min ),
      minLong = Math.floor( bounds.long.min );

  return polyline.map( function( pair ) {
    return pair.map( function( value, i ) {
      return ( ( i % 2 ) ? value - minLong : value - minLat ) * multiplier;
    } );
  } );
};

var drawPolyline = function( encodedPolyline, id ) {
  var decodedPolyline = polyline.decode( encodedPolyline ),
      translatedPolyline = translatePolyline( decodedPolyline ),
      smoothedPolyline = smooth( translatedPolyline );


  var bounds = getPolylineBounds( smoothedPolyline ),
      latDelta = bounds.lat.max - bounds.lat.min + 10,
      longDelta = bounds.long.max - bounds.long.min + 10,
      strokeWidth = Math.max( Math.round( latDelta / 30 ), Math.round( longDelta / 30 ) );

  return new Promise( function( resolve, reject ) {
    gm( multiplier, multiplier, "#FFFFFF00" )
      .stroke( "#FFFFFF")
      .fill( 'transparent' )
      .strokeWidth( strokeWidth )
      .drawPolyline( smoothedPolyline )
      .crop( latDelta, longDelta, Math.floor( bounds.lat.min - 5 ), Math.floor( bounds.long.min - 5 ) )
      .resize( 150, 150 )
      .write( path.resolve( __dirname, id, overlayFilename ), function ( err ) {
        if ( err ) {
          console.log( 'writeOverlay', err );
          return reject( err );
        }
        return resolve();
      } );
  } );
};

var createOverlay = function( id ) {
  var compositePath = path.resolve( __dirname, id, compositeFilename ),
      overlayPath = path.resolve( __dirname, id, overlayFilename ),
      photoPath = path.resolve( __dirname, id, photoFilename );

  return new Promise( function( resolve, reject ) {
    gm( photoPath )
      .composite( overlayPath )
      .gravity( 'NorthEast' )
      .geometry( '+50+50' )
      .write( compositePath, function (err) {
        if ( err ) {
          console.log( 'writeFinished', err );
          return reject( err );
        }
        return resolve();
      } );
  } );
};

var saveImage = function( id ) {
  var s3 = new AWS.S3();
  var compositePath = path.resolve( __dirname, id, compositeFilename );
  var params = {
        Bucket: 'courier-photos',
        Key: id + '.jpg'
      };

  return fs.readFile( compositePath )
    .then( function( file ) {
      params.Body = file;
      return s3.putObject( params ).promise();
    } );
};

var writePhoto = function( data, id, res ) {
  var tmpPath = path.resolve( __dirname, id );
  var photoPath = path.resolve( tmpPath, photoFilename );
  return fs.mkdir( tmpPath )
    .catch( function( error ) {
      // Assume that there's already a file with this name there so we don't need to create image
      var s3Path = 'http://courier-photos.s3-website-eu-west-1.amazonaws.com/' + id + '.jpg';
      console.log( 'Photo saved to ', s3Path );
      res.status( 200 ).send( s3Path );
    } )
    .then( function() {
      return fs.writeFile( photoPath, data, 'binary' );
    } );
};

/**
 * Delete folder created
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
var cleanup = function( id ) {
  return del( [ path.resolve( __dirname, id ) ] );
};

var index = function( req, res, next ) {
  res.send( 200, 'Welcome to the MOS' );
};

var runner = function( req, res, next ) {
  console.log( 'Runner…' );
  var photo = req.query.photo,
      encodedPolyline = req.query.polyline,
      id = req.query.id;

  request( { uri: photo, encoding: 'binary' } )
    .then( function( data ) {
      return writePhoto( data, id, res );
    } )
    .then( function() {
      return drawPolyline( encodedPolyline, id );
    } )
    .then( function () {
      return createOverlay( id );
    } )
    .then( function () {
      return saveImage( id );
    } )
    .then( function () {
      var s3Path = 'http://courier-photos.s3-website-eu-west-1.amazonaws.com/' + id + '.jpg';
      console.log( 'Photo saved to ', s3Path );
      return res.status( 200 ).send( s3Path );
    } )
    .catch( function( error ) {
      console.error( 'Error creating composite image', error );
      return res.status( 500 ).send( new Error( 'Error creating composite image' ) );
    } )
    .then( function () {
      return cleanup( id );
    } )
    .catch( function( error ) {
      console.error( 'Error cleaning up', error );
    } );

};

// Kick it off...
init();

module.exports = app;
