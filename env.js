// External dependencies
var dotenv = require( 'dotenv' );

var environmentName = process.env.NODE_ENV || process.env.node_env || 'development',
    environment;

if ( environmentName === 'development' ) {
  dotenv.load();
}

environment = {
  project: 'map-overlay-service',
  name: environmentName,
  port: process.env.PORT || process.env.port || 2047,
  s3: {
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    region: 'eu-west-1'
  }
};

// Exports
module.exports = environment;