## Installation

### Install ImageMagick
`$ brew update`
`$ brew install imagemagick`

### Install modules
`$ npm install`

## TODO:
- Stream image from url
- Decode polyline
- Draw polyline on image
- Stream to Amazon
- Return the URL